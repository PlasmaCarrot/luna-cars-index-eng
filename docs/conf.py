# Файл конфигурации для сфинкс документации

# Информация о проекте

project = 'VisionLabs LUNA CARS'
copyright = '2021, VisionLabs'
author = 'VisionLabs'

release = '0.1'
version = ''

# Основыне настройки

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['../../_templates']

# Тема для HTML

html_theme = 'sphinx_rtd_theme'

################################
# Настройка плагина с кнопками #
################################

import os

try:
   html_context
except NameError:
   html_context = dict()
html_context['display_lower_left'] = True

# Путь до папки с шаблоном страницы
templates_path = ['../_templates']

# Установка языка
if 'current_language' in os.environ:
   # get the current_language env var set by buildDocs.sh
   current_language = os.environ['current_language']
else:
   # установка языка по умолчанию
   current_language = 'ru'

if 'current_version' in os.environ:
   # get the current_version env var set by buildDocs.sh
   current_version = os.environ['current_version']
else:
   # the user is probably doing `make html`
   # set this build's current version by looking at the branch
   current_version = version

# tell the theme which version we're currently on ('current_version' affects
# the lower-left rtd menu and 'version' affects the logo-area version)
html_context['current_version'] = current_version
html_context['version'] = current_version

# на каком языке собирает данную сборку
html_context['current_language'] = current_language

# Настройка локализации