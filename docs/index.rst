VisionLabs LUNA CARS
===============================

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

VisionLabs LUNA CARS API
----------------------------------

* `VisionLabs LUNA CARS API <http://cars.community.dev.readthedocs.io/projects/apieng/en/latest/>`_ is a vehicle recognition service that allows in real-time following actions.

VisionLabs LUNA CARS Stream
----------------------------------

* `VisionLabs LUNA CARS Stream <http://cars.community.dev.readthedocs.io/projects/streameng/en/latest/>`_ designed for detection and tracking of vehicles and LP in a video stream or detection in images.

VisionLabs LUNA CARS Analytics
----------------------------------

* `VisionLabs LUNA CARS Analytics <http://cars.community.dev.readthedocs.io/projects/analyticseng/en/latest/>`_ designed for detection and tracking of vehicles and license plates in a video stream or detection in images.

The main functions of the system are presented below:

* Detection and tracking of vehicles and license plates;

* recognize license plate;

* determine type of vehicle;

* identify vehicle brands and models;

* determine whether the vehicle belongs to public transport, taxi, or emergency vehicles;

* determine color of vehicle;

* determine country of vehicle registration.

* Display of vehicle and license plate detection events;

1. System Configuration
=====================================

Based on the requirements for the operation of the system, it is possible to install services in various configurations.

1.1. Works with CARS API alone
----------------------------------------

Only the CARS API service is required to process photos of vehicles and license plates and attributes. This configuration allows in real time:

* identify vehicle brands and models;

* detect license plate;

* determine whether the vehicle belongs to public transport, taxi, or emergency vehicles;

* evaluate the quality of incoming images;

* determine the conditions for issuing a fine for vehicles that have violated the speed limit;

* checking the intersection of a solid line;

* determine type of vehicle;

* determine color of vehicle;

* extract the vehicle descriptor;

* determine country of vehicle registration.

The general scheme of the CARS API is shown in Figure 1.

1. Clients send a POST request in JSON format to CARS API via the Nginx balancer, containing information about the required classifiers and base64-encoded images of the vehicle and license plate.

2. CARS API processes the received request and decodes image into a format available for processing, and sequentially runs each passed classifier. 

3. After the end of the work of all classifiers CARS API sends the response in JSON format back to the client. If the passed name of the classifier does not match any of the existing ones in the system, the corresponding message will be returned.

1.2. Works with CARS API and CARS Stream
---------------------------------------------------

If you need to implement vehicle and license plate recognition into an existing analytics and information collection system, you only need to install the CARS API and CARS Stream services.

The main functions of this LUNA CARS configuration are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* determine vehicles attributes;

* Display of results of detection and recognition.

The general scheme of this LUNA CARS configuration is shown in Figure 2.

0. Vehicle appears in frame. 

1. CARS Stream receives an incoming video stream.

2. The incoming video stream is divided into frames in CARS Stream.

3. Frames are sequentially transmitted to the TrackEngine. «TrackEngine» library implements a set of algorithms designed to track the position of one object in a sequence of frames. A separate strategy (Vehicle strategy) is responsible for vehicle detection and tracking, which is configured via a configuration file. TrackEngine contains a list of active tracking and the history of vehicle detections since start track.

4. On incoming frames TrackEngine sequentially launches full detect every 3 frames. The forecast of the vehicle position between detections is provided by the tracking mechanism (calman tracker).

5. TrackEngine buffers incoming frames (buffer size can be set in the «TrackEngine.conf» configuration file, by default 100 frames), as frames are displaced from the buffer, the algorithm for determining the best shot checks if the displaced frame contains a detection of a vehicle that is not blocked by other vehicles and at the same time the detection size is larger than other detections associated with tracking. In this case, the current detection is optimal. In the vehicle recognition area, the identification of the license plate is started. If the license plate is found, the images of the best shot of the car and the LP are sent to CARS API via an HTTP-request.

6. CARS API receives an HTTP request containing a car/license plate pair and runs the following algorithms:

* Recognition of vehicle’s license plate and country of registration;

* Vehicle type and category recognition;

* Vehicle brand and model recognition;

* Launching verification of vehicle's belonging to emergency vehicles or public transport.

7. CARS API sends the recognition results in JSON format to the body of the http request and writes them to a text file suitable for subsequent processing.

8. In the visual mode CARS Stream displays the detection results in the form of a bbox in the interface window with reference to track numbers. In the right part of the screen meta information is displayed with the recognition results obtained from CARS API. The interface window is used only in local operation mode, and the web interface is used in server mode.

9. Data is sent from CARS Stream to external data and analytics system using an HTTP-request.

1.3. Work with CARS API, CARS Stream and CARS Analytics
-----------------------------------------------------------------------

VisionLabs CARS Analytics designed for detection and tracking of vehicles and license plates in a video stream or detection in images. 

The main functions of this configuration are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* determine vehicles attributes;

* Display of results of detection and recognition.

* Display of vehicle and license plate detection events;

* Setting up lists for creating incidents;

* Display of incidents;

* Search for events by incidents;

* Management of user accounts and their access rights;

* Viewing processed video streams from cameras;

* Creation of tasks for search by image and export of search results to a «.xlsx» file.

The general scheme of this LUNA CARS configuration is shown in Figure 3.

0. Vehicle appears in frame. 

1. CARS Stream receives an incoming video stream.

2. The incoming video stream is divided into frames in CARS Stream.

3. Frames are sequentially transmitted to the TrackEngine. «TrackEngine» library implements a set of algorithms designed to track the position of one object in a sequence of frames. A separate strategy (Vehicle strategy) is responsible for vehicle detection and tracking, which is configured via a configuration file. TrackEngine contains a list of active tracking and the history of vehicle detections since start track.

4. On incoming frames TrackEngine sequentially launches full detect every 3 frames. The forecast of the vehicle position between detections is provided by the tracking mechanism (calman tracker).

5. TrackEngine buffers incoming frames (buffer size can be set in the «TrackEngine.conf» configuration file, by default 100 frames), as frames are displaced from the buffer, the algorithm for determining the best shot checks if the displaced frame contains a detection of a vehicle that is not blocked by other vehicles and at the same time the detection size is larger than other detections associated with tracking. In this case, the current detection is optimal. In the vehicle recognition area, the identification of the license plate is started. If the license plate is found, the images of the best shot of the car and the LP are sent to CARS API via an HTTP-request.

6. CARS API receives an HTTP request containing a car/license plate pair and runs the following algorithms:

* Recognition of vehicle’s license plate and country of registration;

* Vehicle type and category recognition;

* Vehicle brand and model recognition;

* Launching verification of vehicle's belonging to emergency vehicles or public transport.

7. CARS API sends the recognition results in JSON format to the body of the http request and writes them to a text file suitable for subsequent processing.

8. In the visual mode CARS Stream displays the detection results in the form of a bbox in the interface window with reference to track numbers. In the right part of the screen meta information is displayed with the recognition results obtained from CARS API. The interface window is used only in local operation mode, and the web interface is used in server mode.

9. Data is sent from CARS Stream to CARS Analytics using an HTTP-request.

10.  In server mode you can create a stream.
